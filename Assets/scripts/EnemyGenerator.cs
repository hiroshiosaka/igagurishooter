﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{



    public GameObject enemyPrefab;

    GameObject[] existEnemys;

    public int maxEnemy = 5;


    void Start()
    {

        existEnemys = new GameObject[maxEnemy];

        StartCoroutine(RepeatGenerate());

    }



    IEnumerator RepeatGenerate()
    {
        while (true)
        {
            GenerateEnemy();
            yield return new WaitForSeconds(3.0f);
        }


        void GenerateEnemy()
        {
            for (int i = 0; i < existEnemys.Length; ++i)
            {
                if (existEnemys[i] == null)
                {

                    existEnemys[i] = Instantiate(enemyPrefab) as GameObject;
                    int enemyX = Random.Range(-20, 20);
                    int enemyZ = Random.Range(-20, 20);
                    existEnemys[i].transform.position = new Vector3(enemyX, 4, enemyZ);

                    return;
                }

            }
        }
    }
}