﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgaguriController : MonoBehaviour
{

    

        public void Shoot(Vector3 dir)
        {
            GetComponent<Rigidbody>().AddForce(dir);
        }

        private void OnCollisionEnter(Collision collision)
        {
            //敵キャラに触れた場合、相手を削除する
            if (collision.gameObject.tag == "Enemy")
            {
                Destroy(collision.gameObject);
            }
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
          //2秒後に削除
            Destroy(this.gameObject, 2.0f);
        }
}